# Awesome TUIs

List of projects that provide terminal user interfaces

## Table of Contents

- [Dashboards](#dashboards)
- [Development](#development)
- [Games](#games)
- [Libraries](#libraries)
- [Messaging](#messaging)
- [Miscellaneous](#miscellaneous)
- [Multimedia](#multimedia)
- [Productivity](#productivity)
- [Web](#web)

## <a name="dashboards"></a>Dashboards

- [cointop](https://github.com/miguelmota/cointop) The fastest and most interactive terminal based UI application for tracking cryptocurrencies
- [dockly](https://github.com/lirantal/dockly) Immersive terminal interface for managing docker containers and services
- [dry](https://github.com/moncho/dry) A Docker manager for the terminal
- [Glances](https://github.com/nicolargo/glances) Glances an Eye on your system. A top/htop alternative.
- [Goaccess](https://github.com/allinurl/goaccess) GoAccess is a real-time web log analyzer and interactive viewer that runs in a terminal in nix systems or through your browser.
- [htop](https://github.com/hishamhm/htop) Interactive text-mode process viewer for Unix systems. It aims to be a better 'top'
- [sen](https://github.com/TomasTomecek/sen) Terminal User Interface for docker engine
- [tdash](https://github.com/jessfraz/tdash) A terminal dashboard with stats from Google Analytics, GitHub, Travis CI, and Jenkins. Very much built specific to me
- [TermUI](https://github.com/gizak/termui) Golang terminal dashboard
- [WTF](https://github.com/senorprogrammer/wtf) The personal information dashboard for your terminal.

## <a name="development"></a>Development

- [amp](https://github.com/jmacdonald/amp) A complete text editor for your terminal
- [grv](https://github.com/rgburke/grv) Terminal interface for viewing git repositories
- [slap](https://github.com/slap-editor/slap) Sublime-like terminal-based text editor
- [tig](https://github.com/jonas/tig) Text-mode interface for git

## <a name="games"></a>Games

- [2048-cli](https://github.com/tiehuis/2048-cli) The game 2048 for your Linux terminal
- [bastet](https://github.com/fph/bastet) Evil falling block game
- [Gameboy Emulator](https://github.com/gabrielrcouto/php-terminal-gameboy-emulator) A PHP Terminal GameBoy Emulator
- [Greed](https://gitlab.com/esr/greed) A game of consumption. Eat as much as you can before munching yourself into a corner!
- [MyMan](https://sourceforge.net/projects/myman/) MyMan is a video game for color and monochrome text terminals in the genre of Namco's Pac-Man
- [NetHack](https://github.com/NetHack/NetHack) dungeon exploration game
- [nInvaders](http://ninvaders.sourceforge.net/) Space Invaders
- [nSnake](https://github.com/alexdantas/nSnake) The classic snake game with textual interface
- [nudoku](https://github.com/jubalh/nudoku) ncurses based sudoku game

## <a name="libraries"></a>Libraries

- [ncurses](https://www.gnu.org/software/ncurses/) 
- [tui-go](https://github.com/marcusolsson/tui-go) A UI library for terminal applications

## <a name="messaging"></a>Messaging

- [Slack-term](https://github.com/erroneousboat/slack-term) Slack client for your terminal
- [TelegramTUI](https://github.com/bad-day/TelegramTUI) Telegram client
- [Weechat](https://weechat.org/) Extensible chat client

## <a name="miscellaneous"></a>Miscellaneous

- [gif-for-cli](https://github.com/google/gif-for-cli)
- [wego](https://github.com/schachmat/wego) Weather app
- [wttr.in](https://github.com/chubin/wttr.in) The right way to check the weather

## <a name="multimedia"></a>Multimedia

- [Image Editor](https://github.com/nhnent/tui.image-editor) Full-featured photo image editor using canvas. It is really easy, and it comes with great filters.
- [mps-youtube](https://github.com/mps-youtube/mps-youtube) Terminal based YouTube player and downloader
- [soundcloud2000](https://github.com/grobie/soundcloud2000) A terminal client for soundcloud
- [timg](https://github.com/hzeller/timg) A terminal image viewer
- [tizonia-openmax-il](https://github.com/tizonia/tizonia-openmax-il) Command-line cloud music player for Linux with support for Spotify, Google Play Music, YouTube, SoundCloud, Dirble, Plex servers and Chromecast devices

## <a name="productivity"></a>Productivity

- [patat](https://github.com/jaspervdj/patat) Terminal-based presentations using Pandoc
- [ranger](https://github.com/ranger/ranger) A VIM-inspired filemanager for the console
- [Visidata](https://github.com/saulpw/visidata) A terminal spreadsheet multitool for discovering and arranging data

## <a name="web"></a>Web

- [browsh](https://github.com/browsh-org/browsh) A fully-modern text-based browser, rendering to TTY and browsers
- [haxor-news](https://github.com/donnemartin/haxor-news) Browse Hacker News like a haxor: A Hacker News command line interface (CLI)
- [Rainbowstream](https://github.com/orakaro/rainbowstream) A smart and nice Twitter client on terminal written in Python
- [rtv](https://github.com/michael-lazar/rtv) Browse Reddit from your terminal
